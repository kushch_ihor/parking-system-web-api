﻿using System;

namespace CarParking.Shared.Exceptions
{
    public class FreeParkingPlaceException : Exception
    {
        public FreeParkingPlaceException(string errorMessage) : base(errorMessage)
        {
        }
    }
}
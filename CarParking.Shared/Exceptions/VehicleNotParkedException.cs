﻿using System;

namespace CarParking.Shared.Exceptions
{
    public class VehicleNotParkedException : Exception
    {
        public VehicleNotParkedException(string errorMessage) : base(errorMessage)
        {

        }
    }
}
﻿using System;
using System.Collections.Generic;

namespace CarParking.Shared.Extensions
{
    public static class VehicleCollectionExtensions
    {
        public static IList<T> UpdateItems<T>(this IList<T> vehicles, Func<T, T> modifyFunc)
        {
            for (var i = 0; i < vehicles.Count; i++)
                vehicles[i] = modifyFunc(vehicles[i]);
            
            return vehicles;
        }
    }
}
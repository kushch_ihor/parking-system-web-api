﻿using System;

namespace CarParking.Shared.Events
{
    public class RemoveVehicleEvent : EventArgs
    {
        public string PhoneNumber { get; }
        public RemoveVehicleEvent(string phoneNumber)
        {
            PhoneNumber = phoneNumber;
        }
    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using CarParking.Shared.Contracts;
using CarParking.Shared.Entities.Abstracts;
using CarParking.Shared.Exceptions;

namespace CarParking.Shared.Entities
{
    public class ParkingStorage : IParkingStorage
    {
        private static ParkingStorage _storage;
        public static decimal Balance { get; private set; } = ParkingSettings.Balance;

        public IList<Vehicle> VehicleCollection { get; }

        private ParkingStorage()
        {
            VehicleCollection = new List<Vehicle>(ParkingSettings.Capacity);
        }

        public bool IsStorageFull => VehicleCollection.Count <= ParkingSettings.Capacity;

        public static ParkingStorage CreateParkingStorage()
        {
            return _storage ?? (_storage = new ParkingStorage());
        }

        public void AddBalance(decimal value)
        {
            if (value <= 0)
                throw new ArgumentException("You are trying to use negative value");

            Balance += value;
        }

        public void AddVehicleToParking(Vehicle vehicle)
        {
            if (VehicleCollection.Count <= ParkingSettings.Capacity)
                VehicleCollection.Add(vehicle);
            else
                throw new FreeParkingPlaceException("Sorry but parking is full");
        }

        public void RemoveVehicleFromParking(Guid vehicleId)
        {
            var vehicle = VehicleCollection.FirstOrDefault(o => o.VehicleId == vehicleId);

            if (vehicle == null)
                throw new VehicleNotParkedException("Your vehicle was not parked there.");

            if (vehicle.Balance < 0)
                throw new NegativeVehicleBalanceException("Your vehicle has negative balance.");
            
            VehicleCollection.Remove(vehicle);
        }
    }
}
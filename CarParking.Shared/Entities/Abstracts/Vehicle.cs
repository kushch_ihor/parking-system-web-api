﻿using System;
using CarParking.Shared.Enums;
using CarParking.Shared.Extensions;

namespace CarParking.Shared.Entities.Abstracts
{
    public abstract class Vehicle
    {
        public abstract VehicleEnum Type { get; }

        public Guid VehicleId { get; }

        public abstract decimal Rate { get; }

        public string OwnerPhone { get; }

        protected Vehicle(string phone)
        {
            if (!phone.IsValidPhoneNumber())
                throw new ArgumentException("You have entered invalid phone number.");

            OwnerPhone = phone;
            VehicleId = Guid.NewGuid();
        }

        public decimal Balance { get; set; }

    }
}

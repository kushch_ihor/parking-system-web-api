﻿using System;
using CarParking.Shared.Contracts;

namespace CarParking.Shared.Entities
{
    public class ParkingTrasaction : IParkingTransaction
    {
        public Guid Id { get; }
        public DateTime TransactionTime { get; }
        public Guid VehicleId { get; }
        public decimal TransactionAmount { get; set; }

        public ParkingTrasaction(Guid vehicleId)
        {
            Id = Guid.NewGuid();
            TransactionTime = DateTime.Now;
            VehicleId = vehicleId;
        }
    }
}
﻿using System;
using System.Collections.Generic;
using CarParking.Shared.Entities.Abstracts;

namespace CarParking.Shared.Contracts
{
    public interface IParkingStorage
    {
        IList<Vehicle> VehicleCollection { get; }
        bool IsStorageFull { get; }
        void AddBalance(decimal value);
        void AddVehicleToParking(Vehicle vehicle);
        void RemoveVehicleFromParking(Guid vehicleId);
    }
}
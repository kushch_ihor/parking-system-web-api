﻿using System.Collections.Generic;
using System.Threading.Tasks;
using CarParking.Shared.Entities.Abstracts;

namespace CarParking.Shared.Contracts.Services
{
    public interface IParkingHandler
    {
        int GetFreePlacesCount();
        IList<Vehicle> GetAllVehicles();
    }
}
﻿using System.Collections.Generic;
using CarParking.Shared.Entities.Abstracts;

namespace CarParking.Shared.Contracts.Services
{
    public interface IFinanceService
    {
        decimal GetParkingBalance();
        decimal GetParkingBalanceInLastMinute(IList<IParkingTransaction> transactions);
        decimal GetVehicleBalance(Vehicle vehicle);
        void AddVehicleBalance(Vehicle vehicle, decimal value);
    }
}
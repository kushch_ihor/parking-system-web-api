﻿using System.Collections.Generic;

namespace CarParking.Shared.Contracts.Services
{
    public interface ITransactionService
    {
        IList<IParkingTransaction> TransactionsInLastMinute { get; set; }
        IEnumerable<IParkingTransaction> GetTransactionsInLastMinute();
    }
}
﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using CarParking.Shared.Contracts;
using CarParking.Shared.Contracts.Services;

namespace CarParking.Services
{
    public class LoggingService : ILoggingService
    {
        private readonly string _pathToFile;
        private readonly string _fileName = "Transactions.log";
        private readonly char _delimiter = '&';

        private string _transactionId = "Id",
            _vehicleId = "VehicleId",
            _time = "Time",
            _amount = "Amount";

        public LoggingService(string pathToFile = null)
        {
            _pathToFile = pathToFile ?? Directory.GetCurrentDirectory();
        }

        public void WriteTransactionToFile(IList<IParkingTransaction> transactions)
        {
            try
            {
                using (var stream = new StreamWriter(Path.Combine(_pathToFile, _fileName), true))
                {
                    foreach (var transaction in transactions)
                    {
                        stream.WriteLine($"{_transactionId}={transaction.Id.ToString()}" +
                                         $"{_delimiter}{_vehicleId}={transaction.VehicleId}" +
                                         $"{_delimiter}{_time}={transaction.TransactionTime:YYYY:DD:MM:hh:mm:ss}" +
                                         $"{_delimiter}{_amount}={transaction.TransactionAmount.ToString(CultureInfo.InvariantCulture)}");
                    }
                }
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
            }
        }

        public async Task WriteTransactionToFileAsync(IList<IParkingTransaction> transactions)
        {
            try
            {
                using (var stream = new StreamWriter(Path.Combine(_pathToFile, _fileName), true))
                {
                    foreach (var transaction in transactions)
                    {
                        await stream.WriteLineAsync($"{_transactionId}={transaction.Id.ToString()}" +
                                                    $"{_delimiter}{_vehicleId}={transaction.VehicleId}" +
                                                    $"{_delimiter}{_time}={transaction.TransactionTime:YYYY:DD:MM:hh:mm:ss}" +
                                                    $"{_delimiter}{_amount}={transaction.TransactionAmount.ToString(CultureInfo.InvariantCulture)}");
                    }
                }
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
            }

        }

        public void PrintAllTransactionsFromFile()
        {
            try
            {
                if (File.Exists(Path.Combine(_pathToFile, _fileName)))
                {
                    using (var stream = new StreamReader(Path.Combine(_pathToFile, _fileName)))
                    {
                        var result = stream.ReadToEnd();
                        Console.WriteLine(result);
                    }
                }
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
            }
        }

        public async Task PrintAllTransactionsFromFileAsync()
        {
            try
            {
                if (File.Exists(Path.Combine(_pathToFile, _fileName)))
                {
                    using (var stream = new StreamReader(Path.Combine(_pathToFile, _fileName)))
                    {
                        var result = await stream.ReadToEndAsync();
                        Console.WriteLine(result);
                    }
                }
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
            }
        }

        public void PrintTransactions(IList<IParkingTransaction> transactions)
        {
            try
            {
                foreach (var transaction in transactions)
                {
                    Console.WriteLine($"{_transactionId}={transaction.Id.ToString()}" +
                                      $"{_delimiter}{_vehicleId}={transaction.VehicleId}" +
                                      $"{_delimiter}{_time}={transaction.TransactionTime:YYYY:DD:MM:hh:mm:ss}" +
                                      $"{_delimiter}{_amount}={transaction.TransactionAmount.ToString(CultureInfo.InvariantCulture)}");
                }
                if (!transactions.Any())
                {
                    Console.WriteLine(@"Transactions hasn't present");
                }
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
            }
        }
    }
}